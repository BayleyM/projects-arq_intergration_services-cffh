﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.IO;

namespace CFFH_intergration
{
    public partial class DataTableHelper
    {
        // Public properties.
        public string TableStyle = "border-width: 1px; border-collapse: collapse;";
        public string TrStyle = "padding: 2px; background-color: #eeeeff; font-size: medium;";
        public string TrOddStyle = "padding: 2px;background-color: #ddddff; font-size: medium";
        public string ThStyle = "padding: 8px; font-weight: bold; background-color: #0066CC; color: #ffffff; font-size: 16px;";
        public string TdStyle = "";
        public string TdOddStyle = "";

        public string TableClass = "";
        public string TrClass = "";
        public string TrOddClass = "";
        public string ThClass = "";
        public string TdClass = "";
        public string TdOddClass = "";


        // ---------------------------------------------------------------------
        /// <summary>
        /// Produce HTML encoding of a DataTable with no formatting
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public string DataTableToHtmlTable(DataTable data)
        {
            return DataTableToHtmlTable(data,
                                        TableStyle, // tableStyle,
                                        ThStyle, //headerStyle,
                                        TrStyle, //     oddRowStyle,
                                        TrOddStyle,//     evenRowStyle,
                                     
                                        "",  //     tableCssClass,
                                        "",  //     headerCssClass,
                                        "",  //     oddRowCssClass,
                                        ""); //     evenRowCssClass)
        }

#if __OFF__
        // ---------------------------------------------------------------------
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="tableStyle"></param>
        /// <param name="headerStyle"></param>
        /// <param name="oddRowStyle"></param>
        /// <param name="evenRowStyle"></param>
        /// <param name="tableCssClass"></param>
        /// <param name="headerCssClass"></param>
        /// <param name="oddRowCssClass"></param>
        /// <param name="evenRowCssClass"></param>
        /// <returns></returns>
        public string DataTableToHtmlTable(DataTable data)
        {
            return DataTableToHtmlTable(data,
                                        "border-width: 1px; border-collapse: collapse;", // tableStyle,
                                        "background-color: #0066CC; color: #ffffff; font-size: small;", //headerStyle,
                                        "background-color: #eeeeff; font-size: small;", //     oddRowStyle,
                                         "background-color: #ddddff; font-size: small;", //     evenRowStyle,
                                         "", //     tableCssClass,
                                         "", //     headerCssClass,
                                         "", //     oddRowCssClass,
                                         ""); //     evenRowCssClass)
        }
#endif

        // ---------------------------------------------------------------------
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="tableStyle"></param>
        /// <param name="headerStyle"></param>
        /// <param name="oddRowStyle"></param>
        /// <param name="evenRowStyle"></param>
        /// <param name="tableCssClass"></param>
        /// <param name="headerCssClass"></param>
        /// <param name="oddRowCssClass"></param>
        /// <param name="evenRowCssClass"></param>
        /// <returns></returns>
        public string DataTableToHtmlTable(DataTable data,
                                            string tableStyle,
                                            string headerStyle,
                                            string oddRowStyle,
                                            string evenRowStyle,
                                            string tableCssClass,
                                            string headerCssClass,
                                            string oddRowCssClass,
                                            string evenRowCssClass)
        {
            string html = TableElementWithStyle(tableStyle,
                                               tableCssClass);

            // Table header.
            html += "<tr>";
            foreach (DataColumn col in data.Columns)
            {
                html += "<th";
                if (headerCssClass != String.Empty)
                {
                    html += " class='" + headerCssClass + "'";
                }
                if (headerStyle != String.Empty)
                {
                    html += " style='" + headerStyle + "'";
                }
                html += ">";
                html += col.ColumnName.ToString();
                html += "</th>";
            }
            html += "</tr>";

            // Table data.
            bool oddRow = true;
            foreach (DataRow row in data.Rows)
            {
                // </tr>
                html += TrElementWithStyle(oddRow,
                                           oddRowStyle,
                                           evenRowStyle,
                                           oddRowCssClass,
                                           evenRowCssClass);

                // Row data.
                foreach (DataColumn col in data.Columns)
                {
                    html += TdElementWithStyle(null, null); //"<td>"; 
                    string colName = col.ColumnName.ToString();
                    switch (col.DataType.Name)
                    {
                        case "DateTime":
                            DateTime date = DateTime.MinValue;
                            if (DateTime.TryParse(row[colName].ToString(), out date))
                            {
                                html += date.ToString("dd-MMM-yyyy");
                            }
                            else
                            {
                                html += "Invalid date (" + row[colName].ToString() + ")";
                            }
                            break;
                        default:
                            html += row[colName].ToString();
                            break;
                    }

                    html += "</td>";
                }

                html += "</tr>";

                if (oddRow) { oddRow = false; }
                else {oddRow=true;}

            }

            // Table footer.
            //html += "<tr";
            //if (headerCssClass != String.Empty)
            //{
            //    html += " class='" + headerCssClass + "'";
            //}
            //if (headerStyle != String.Empty)
            //{
            //    html += " style='" + headerStyle + "'";
            //}
            //html += ">";

            //foreach (DataColumn col in data.Columns)
            //{
            //    html += "<th>";
            //    html += col.ColumnName.ToString();
            //    html += "</th>";
            //}
            //html += "</tr>";

            html += "</table>";

            return html;
        }

        // ---------------------------------------------------------------------
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableStyle"></param>
        /// <param name="tableCssClass"></param>
        /// <returns></returns>
        private string TableElementWithStyle(string tableStyle,
                                             string tableCssClass)
        {
            string t = "<table";

            // Table style - defaults.
            string theClass = TableClass;
            string theStyle = TableStyle;

            // Table style - overriding defaults.
            if (string.IsNullOrEmpty(tableCssClass))
            {
                theClass = tableCssClass;
            }
            if (string.IsNullOrEmpty(tableStyle))
            {
                theStyle = tableStyle;
            }

            // Final markup.
            if (string.IsNullOrEmpty(theClass))
            {
                t += " class='" + theClass + "'";
            }
            if (string.IsNullOrEmpty(theStyle))
            {
                t += " style='" + theStyle + "'";
            }
            t += ">";

            return t;
        }

        // ---------------------------------------------------------------------
        /// <summary>
        /// 
        /// </summary>
        /// <param name="oddRow"></param>
        /// <param name="oddRowStyle"></param>
        /// <param name="evenRowStyle"></param>
        /// <param name="oddRowCssClass"></param>
        /// <param name="evenRowCssClass"></param>
        /// <returns></returns>
        private string TrElementWithStyle(bool oddRow,
                                          string oddRowStyle,
                                          string evenRowStyle,
                                          string oddRowCssClass,
                                          string evenRowCssClass)
        {
            // Choose row style.
            string rowStyle = "";
            string rowClass = "";
            if (oddRow)
            {
                rowStyle = oddRowStyle;
                rowClass = oddRowCssClass;
            }
            else
            {
                rowStyle = evenRowStyle;
                rowClass = evenRowCssClass;
            }
            oddRow = !oddRow;

            string tr = "<tr";

            if (rowClass != String.Empty)
            {
                tr += " class='" + rowClass + "'";
            }

            if (rowStyle != String.Empty)
            {
                tr += " style='" + rowStyle + "'";
            }

            tr += ">";

            return tr;
        }

        // ---------------------------------------------------------------------
        /// <summary>
        /// TH element style
        /// </summary>
        /// <param name="tdStyle"></param>
        /// <param name="tdCssClass"></param>
        /// <returns></returns>
        private string ThElementWithStyle(string thStyle,
                                          string thCssClass)
        {
            string t = "<td";

            // TH style - defaults.
            string theClass = TdClass;
            string theStyle = TdStyle;

            // TH style - overriding defaults.
            if (string.IsNullOrEmpty(thCssClass))
            {
                theClass = thCssClass;
            }
            if (string.IsNullOrEmpty(thStyle))
            {
                theStyle = thStyle;
            }

            // Final markup.
            if (string.IsNullOrEmpty(theClass))
            {
                t += " class='" + theClass + "'";
            }
            if (string.IsNullOrEmpty(theStyle))
            {
                t += " style='" + theStyle + "'";
            }
            t += ">";

            return t;
        }

        // ---------------------------------------------------------------------
        /// <summary>
        /// TD element style
        /// </summary>
        /// <param name="tdStyle"></param>
        /// <param name="tdCssClass"></param>
        /// <returns></returns>
        private string TdElementWithStyle(string tdStyle,
                                          string tdCssClass)
        {
            string t = "<td";

            // TD style - defaults.
            string theClass = TdClass;
            string theStyle = TdStyle;            

            // TD style - overriding defaults.
            if (string.IsNullOrEmpty(tdCssClass))
            {
                theClass = tdCssClass;
            }
            if (string.IsNullOrEmpty(tdStyle))
            {
                theStyle = tdStyle;
            }

            // Final markup.
            if (string.IsNullOrEmpty(theClass))
            {
                t += " class='" + theClass + "'";
            }
            if (string.IsNullOrEmpty(theStyle))
            {
                t += " style='" + theStyle + "'";
            }
            t += ">";

            return t;
        }

    }
}
