﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CFFH_intergration
{
    public class CRISexam
    {
        public string HospID { get; set; }
        public DateTime ExamDate { get; set; }
        public DateTime ReportedDate { get; set; }
        public string ExamType { get; set; }
        public string Status { get; set; }
        public string Location { get; set; }
    }
}

