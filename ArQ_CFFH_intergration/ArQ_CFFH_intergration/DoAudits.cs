﻿using System.Text;
using System.Data;
using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using ArQ_CFFH_intergration.GetRISData;



namespace CFFH_intergration
{
    class DoAudits
    {
        StringBuilder _errors = new StringBuilder();
        StringBuilder _CRISinfo = new StringBuilder();
        StringBuilder _auditQueriesInfo = new StringBuilder();
        StringBuilder _CRISinfo_email = new StringBuilder();
        StringBuilder _auditQueriesInfo_email = new StringBuilder();


        DataTable _CrisinfoTabMamos = new DataTable();
        DataTable _CrisinfoTabMRIs = new DataTable();
        DataTable _CrisinfoTabCancels = new DataTable();
        DataTable _CrisinfoTabExtras = new DataTable();

        DataTable _qu_missingActiveMammo = new DataTable();
        DataTable _qu_missingActiveMRI = new DataTable();
        DataTable _qu_deceasedCurrent = new DataTable();
        DataTable _qu_missingWRMammoCurrent = new DataTable();
        DataTable _qu_OldWRMammoCurrent = new DataTable();
        DataTable _qu_noMammosOrProtocol = new DataTable();
        DataTable _qu_CurrentButWithEndApptMammo = new DataTable();
        DataTable _qu_QueryNonCurrentButNoDischargeAndFutureApptsMammo = new DataTable();
        DataTable _qu_RecentlyEnded = new DataTable();


        Dictionary<string, List<CRISexam>> _CRIS_Mammos = new Dictionary<string, List<CRISexam>>();
        Dictionary<string, List<CRISexam>> _CRIS_MRIs = new Dictionary<string, List<CRISexam>>();
        Dictionary<string, List<CRISexam>> _CRIS_CANCELS = new Dictionary<string, List<CRISexam>>();

        Dictionary<string, List<DateTime>> _ArQ_Patients_Appt_let_sent_mammo = new Dictionary<string, List<DateTime>>();
        Dictionary<string, List<DateTime>> _ArQ_Patients_Appt_let_sent_mri = new Dictionary<string, List<DateTime>>();
        List<string> _ArQ_Patients_All_Current = new List<string>();

        Dictionary<string, List<DateTime>> nextmammo_fix = new Dictionary<string, List<DateTime>>();
        Dictionary<string, int> nextmammo_status = new Dictionary<string, int>();



        Dictionary<string, string> _cancel_codes = new Dictionary<string, string>();
        Dictionary<string, string> _scanCodeToType = new Dictionary<string, string>();

        public string Errors
        {
            get { return _errors.ToString(); }
        }

        public string Get_CRIS_Results_Reminders
        {
            get { return _CRISinfo.ToString(); }
        }
  
        public string Get_Audit_Query_Warnings
        {
            get { return _auditQueriesInfo.ToString(); }
        }

        public void Go(int nDays, int nDaysBackStart, bool outWithResultsOnly)
        {
 
            _cancel_codes.Add("CH", "Hosp Cancel");
            _cancel_codes.Add("CHB", "Booking Error");
            _cancel_codes.Add("CHBU", "Beds Unavailable");
            _cancel_codes.Add("CHDU", "Duplicate request");
            _cancel_codes.Add("CHUN", "Unjustified examination");
            _cancel_codes.Add("CP", "Patient Cancel");
            _cancel_codes.Add("CPCDNA", "1st DNA");
            _cancel_codes.Add("CPCGA", "Patient Change");
            _cancel_codes.Add("CPD", "Patient deceased");
            _cancel_codes.Add("CPDNA2", "2nd DNA");
            _cancel_codes.Add("CPGDNA", "GP DNA");
            _cancel_codes.Add("CPN", "Pat no longer wants the exam");
            _cancel_codes.Add("CPNR", "Partial Book - Non responder");

            _scanCodeToType.Add("XMAMB", "Mammogram");
            _scanCodeToType.Add("MMAMB", "MRI");
            _scanCodeToType.Add("MMAMBC", "MRI");

            fix_nextmammos();

            setupRISTable(_CrisinfoTabMamos,false,false);
            setupRISTable(_CrisinfoTabMRIs,false,false);
            setupRISTable(_CrisinfoTabExtras, false,true);
            setupRISTable(_CrisinfoTabCancels,true,false);

            setupQueryTable(_qu_deceasedCurrent);
            setupQueryTable(_qu_missingActiveMammo);
            setupQueryTable(_qu_missingActiveMRI);
            setupQueryTable(_qu_missingWRMammoCurrent);
            setupQueryTable(_qu_OldWRMammoCurrent);
            setupQueryTable(_qu_noMammosOrProtocol);
            setupQueryTable(_qu_CurrentButWithEndApptMammo);
            setupQueryTable(_qu_QueryNonCurrentButNoDischargeAndFutureApptsMammo);
            setupQueryTableEnd(_qu_RecentlyEnded);

            _CRISinfo.Append(Get_CRIS_Lists(nDays, nDaysBackStart, outWithResultsOnly));
            if (outWithResultsOnly)
            {
                _auditQueriesInfo.Append(Get_audit_querys());
            }

        }

        private void fix_nextmammos()
        {
            string errormsg;

            DataTable AllCurrentappts = QueryAllCurrentMammoWR(out errormsg);
            DateTime hold_root_next;
            DateTime hold_appt_next;
            string  hospID;
            int statusCode;

            for (int i = 0; i < AllCurrentappts.Rows.Count; i++)
            {
                if (!DateTime.TryParse(AllCurrentappts.Rows[i]["ref_next_mammo$date"].ToString(), out hold_root_next))
                {
                    continue;
                }
                if (!DateTime.TryParse(AllCurrentappts.Rows[i]["scan_date_mammo$date"].ToString(), out hold_appt_next))
                {
                    continue;
                }
                //if (!int.TryParse(AllCurrentappts.Rows[i]["primarykey"].ToString(), out pk))
                //{
                //    continue;
                //}
                if (string.IsNullOrEmpty(AllCurrentappts.Rows[i]["demog$hospital_id"].ToString()))
                {
                    continue;
                }
                else
                {
                    hospID = AllCurrentappts.Rows[i]["demog$hospital_id"].ToString();
                }
                if (!int.TryParse(AllCurrentappts.Rows[i]["status_mammo$item"].ToString(), out statusCode))
                {
                    continue;
                }

                //
                if (nextmammo_fix.ContainsKey(hospID))
                {

                    if (hold_appt_next < nextmammo_fix[hospID][0])
                    {
                        nextmammo_fix[hospID][0] = hold_appt_next;
                        nextmammo_fix[hospID][1] = hold_root_next;
                        nextmammo_status[hospID] = statusCode;
                    }
                }
                else
                {
                    List<DateTime> newadddates = new List<DateTime>();
                    newadddates.Add(hold_appt_next);
                    newadddates.Add(hold_root_next);

                    nextmammo_fix.Add(hospID, newadddates);
                    nextmammo_status.Add(hospID, statusCode);
                }
            }
            string tempSt=string.Empty;
            foreach (KeyValuePair<string, List<DateTime>> kvp in nextmammo_fix)
            {
                string thiserror=string.Empty;
                if (kvp.Value[0] != kvp.Value[1])
                {
                    setFieldInTable("roottable", "demog$hospital_id", kvp.Key, "ref_next_mammo$date", kvp.Value[0], out thiserror);
                    tempSt+=kvp.Key + " , " + kvp.Value[0].ToString("yyyy-MM-dd") + " , " + kvp.Value[1].ToString("yyyy-MM-dd")+" , "+ nextmammo_status[kvp.Key].ToString() + Environment.NewLine;
                    if (!string.IsNullOrEmpty(thiserror))
                    {
                        errormsg += thiserror;
                    }
                }
            }
        }

        private string Get_CRIS_Lists(int nDays, int startDaysBackOffset, bool outWithResultsOnly)
        {
            string returnText = "";
            string CRISReadError = setup_CRIS_info_from_service(nDays, startDaysBackOffset,outWithResultsOnly);
            string ArqApptLetSentQueryErrorMammo = setupArqApptLetterSentMammo();
            string ArqApptLetSentQueryErrorMRI = setupArqApptLetterSentMRI();
            //string ArQAllCurrentQueryError = setupArqAllCurrent();
            string ArQAllWithoutWindowAppt = setupArqAllCurrentWitoutTimeWinAPPt(nDays, startDaysBackOffset); 

            _errors.AppendLine(CRISReadError);
            _errors.AppendLine(ArqApptLetSentQueryErrorMammo);
            _errors.AppendLine(ArqApptLetSentQueryErrorMRI);
            _errors.AppendLine(ArQAllWithoutWindowAppt);


            DateTime endDateWindow = DateTime.Now;
            endDateWindow = endDateWindow.AddDays(-startDaysBackOffset);
            DateTime startDateWindow = endDateWindow.AddDays(-nDays);

            if (outWithResultsOnly)
            {

                string mammoExpectedText = "<hr/>MAMMO Results,  found on RIS  - Expected in ArQ" + Environment.NewLine + Environment.NewLine;
                string mriExpectedText = "<hr/>MRI Results, found on RIS - Expected in ArQ" + Environment.NewLine + Environment.NewLine;
                string cancelsText = "<hr/>Mammo and MRI patient cancelled on RIS, That are expected in ArQ" + Environment.NewLine + Environment.NewLine;
                string allExtrasText = "<hr/>Possible Fast-Track Patients - Results on RIS, NOT Expected in ArQ (From " + startDateWindow.ToString("dd/MM/yyyy") ;
                allExtrasText += " To " + endDateWindow.ToString("dd/MM/yyyy") + ")" + Environment.NewLine + Environment.NewLine;
                
                int mammoCnt = 0, mriCnt = 0, cancelCnt = 0, thisCancel = 0, extrasCnt =0;

                if (_ArQ_Patients_Appt_let_sent_mammo.Count > 0)
                {
                    foreach (KeyValuePair<string, List<DateTime>> kvp in _ArQ_Patients_Appt_let_sent_mammo)
                    {
                        string hospID = kvp.Key;
                        List<DateTime> examDates = kvp.Value;
                        foreach (DateTime examDate in examDates)
                        {
                            if (_CRIS_Mammos.ContainsKey(hospID))
                            {
                                foreach (CRISexam exam in _CRIS_Mammos[hospID])
                                {
                                    DataRow myRow = _CrisinfoTabMamos.NewRow();
                                    myRow["  Hospital ID  "] = "<a href=\"?uid=" + hospID + "&group=roottable&page=mammo\">" + hospID + "</a>";
                                    myRow["  ArQ Scan Date  "] = examDate;
                                    myRow["  RIS Reported Date  "] = exam.ReportedDate;
                                    myRow["  Location  "] = exam.Location;
                                    //myRow["  Status Code  "] = exam.Status;
                                    _CrisinfoTabMamos.Rows.Add(myRow);

                                    mammoCnt++;

                                    if (!ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                                    {
                                       
                                        mammoExpectedText += hospID + " ArQ mammo date: " + examDate.ToString("dd:MM:yyyy");
                                        mammoExpectedText += "   -   RIS date reported:" + exam.ReportedDate.ToString("dd:MM:yyyy") + " Location:" + exam.Location + " StatusCode:" + exam.Status + Environment.NewLine;
                                    }
                                }
                            }
                            thisCancel = 0;
                            cancelsText += process_CRIS_CANCELS(hospID, examDate, out thisCancel);
                            cancelCnt += thisCancel;

                        }
                    }
                }
               
                if (_ArQ_Patients_Appt_let_sent_mri.Count > 0)
                {
                    foreach (KeyValuePair<string, List<DateTime>> kvp in _ArQ_Patients_Appt_let_sent_mri)
                    {
                        string hospID = kvp.Key;
                        List<DateTime> examDates = kvp.Value;
                        foreach (DateTime examDate in examDates)
                        {
                            if (_CRIS_MRIs.ContainsKey(hospID))
                            {

                                foreach (CRISexam exam in _CRIS_MRIs[hospID])
                                {
                                    DataRow myRow = _CrisinfoTabMRIs.NewRow();
                                    myRow["  Hospital ID  "] = "<a href=\"?uid=" + hospID + "&group=roottable&page=scans\">" + hospID + "</a>";
                                    myRow["  ArQ Scan Date  "] = examDate;
                                    myRow["  RIS Reported Date  "] = exam.ReportedDate;
                                    myRow["  Location  "] = exam.Location;
                                    //myRow["  Status Code  "] = exam.Status;
                                    _CrisinfoTabMRIs.Rows.Add(myRow);

                                    mriCnt++;
                                    if (!ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                                    {

                                        mriExpectedText += hospID + " ArQ MRI date: " + examDate.ToString("dd:MM:yyyy");
                                        mriExpectedText += "   -   RIS date reported: " + exam.ReportedDate.ToString("dd:MM:yyyy") + " Location:" + exam.Location + " StatusCode:" + exam.Status + Environment.NewLine;
                                    }
                                }
                            }
                            thisCancel = 0;
                            cancelsText += process_CRIS_CANCELS(hospID, examDate, out thisCancel);
                            cancelCnt += thisCancel;
                        }
                    }
                }
                if (_ArQ_Patients_All_Current.Count > 0)   // Now search for possible extra FAST-TRACK patients
                {
                    foreach (string extraToCheck in _ArQ_Patients_All_Current)
                    {
                        if((!_ArQ_Patients_Appt_let_sent_mammo.ContainsKey(extraToCheck))&&(_CRIS_Mammos.ContainsKey(extraToCheck)))
                        {
                            foreach (CRISexam exam in _CRIS_Mammos[extraToCheck])
                            {
                                DataRow myRow = _CrisinfoTabExtras.NewRow();
                                myRow["  Hospital ID  "] = "<a href=\"?uid=" + extraToCheck + "&group=roottable&page=mammo\">" + extraToCheck + "</a>";
                                myRow["  RIS Scan Date  "] = exam.ExamDate;
                                myRow["  RIS Reported Date  "] = exam.ReportedDate;
                                myRow["  Location  "] = exam.Location;
                                //myRow["  Status Code  "] = exam.Status;
                                myRow["  Exam Type  "] = _scanCodeToType[exam.ExamType];
                                _CrisinfoTabExtras.Rows.Add(myRow);

                                extrasCnt++;

                                if (!ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                                {

                                    allExtrasText += extraToCheck + " ArQ mammo date: " + exam.ExamDate.ToString("dd:MM:yyyy");
                                    allExtrasText += "   -   RIS date reported:" + exam.ReportedDate.ToString("dd:MM:yyyy") + " Location:" + exam.Location + " StatusCode:" + exam.Status + Environment.NewLine;
                                }
                            }
                        }
                        if((!_ArQ_Patients_Appt_let_sent_mri.ContainsKey(extraToCheck))&&(_CRIS_MRIs.ContainsKey(extraToCheck)))
                        {
                            foreach (CRISexam exam in _CRIS_MRIs[extraToCheck])
                            {
                                DataRow myRow = _CrisinfoTabExtras.NewRow();
                                myRow["  Hospital ID  "] = "<a href=\"?uid=" + extraToCheck + "&group=roottable&page=scans\">" + extraToCheck + "</a>";
                                myRow["  RIS Scan Date  "] = exam.ExamDate;
                                myRow["  RIS Reported Date  "] = exam.ReportedDate;
                                myRow["  Location  "] = exam.Location;
                                //myRow["  Status Code  "] = exam.Status;
                                myRow["  Exam Type  "] = exam.ExamType;
                                _CrisinfoTabExtras.Rows.Add(myRow);

                                extrasCnt++;

                                if (!ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                                {

                                    allExtrasText += extraToCheck + " ArQ mri date: " + exam.ExamDate.ToString("dd:MM:yyyy");
                                    allExtrasText += "   -   RIS date reported:" + exam.ReportedDate.ToString("dd:MM:yyyy") + " Location:" + exam.Location + " StatusCode:" + exam.Status + Environment.NewLine;
                                }
                            }
                        }
                    }
                }





                if (ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                {
                    if (mammoCnt > 0)
                    {
                        DataTableHelper DTHMammo = new DataTableHelper();
                        _CrisinfoTabMamos.DefaultView.Sort = "[  RIS Reported Date  ] ASC";
                        _CrisinfoTabMamos = _CrisinfoTabMamos.DefaultView.ToTable();
                        mammoExpectedText += DTHMammo.DataTableToHtmlTable(_CrisinfoTabMamos) + Environment.NewLine;
                    }
                    if (mriCnt > 0)
                    {

                        DataTableHelper DTHmri = new DataTableHelper();
                        _CrisinfoTabMRIs.DefaultView.Sort = "[  RIS Reported Date  ] ASC";
                        _CrisinfoTabMRIs = _CrisinfoTabMRIs.DefaultView.ToTable();
                        mriExpectedText += DTHmri.DataTableToHtmlTable(_CrisinfoTabMRIs) + Environment.NewLine;
                    }
                    if (cancelCnt > 0)
                    {
                        DataTableHelper DTHcancels = new DataTableHelper();
                        _CrisinfoTabCancels.DefaultView.Sort = "[  ArQ Scan Date  ] ASC";
                        _CrisinfoTabCancels = _CrisinfoTabCancels.DefaultView.ToTable();
                        cancelsText += DTHcancels.DataTableToHtmlTable(_CrisinfoTabCancels) + Environment.NewLine;
                    }
                    if (extrasCnt > 0)
                    {
                        DataTableHelper DTHextras = new DataTableHelper();
                        _CrisinfoTabExtras.DefaultView.Sort = "[  RIS Reported Date  ] ASC";
                        _CrisinfoTabExtras = _CrisinfoTabExtras.DefaultView.ToTable();
                        allExtrasText += DTHextras.DataTableToHtmlTable(_CrisinfoTabExtras) + Environment.NewLine;
                    }
                }

                if (mammoCnt == 0)
                {
                    mammoExpectedText += "      - No expected Mammo results found - " + Environment.NewLine;
                }
                if (mriCnt == 0)
                {
                    mriExpectedText += "      - No expected MRI results found -" + Environment.NewLine;
                }
                if (cancelCnt == 0)
                {
                    cancelsText += "      - No expected cancelled scans found - " + Environment.NewLine;
                }
                if (extrasCnt == 0)
                {
                    allExtrasText += "      - No unexpected ArQ patient scans found - " + Environment.NewLine;
                }


                returnText += mammoExpectedText + Environment.NewLine + mriExpectedText + Environment.NewLine + cancelsText + Environment.NewLine + allExtrasText+Environment.NewLine;
            }
            else
            {
                returnText += "HospID,Forename,Surname,DOB,Examcode,ExamDate,Location" + Environment.NewLine;
                foreach (KeyValuePair<string, List<DateTime>> kvp in _ArQ_Patients_Appt_let_sent_mammo)
                {
                    string hospID = kvp.Key;
                    List<DateTime> examDates = kvp.Value;
                    foreach (DateTime examDate in examDates)
                    {
                        if (_CRIS_Mammos.ContainsKey(hospID))
                        {
                            foreach (CRISexam exam in _CRIS_Mammos[hospID])
                            {
                                returnText += hospID + ", , , ," + exam.ExamType.ToString() + ", " + exam.ExamDate.ToString("yyyy-MM-dd");
                                returnText += ", " + exam.Location.ToString() + Environment.NewLine;
                            }
                        }
                    }
                }
                foreach (KeyValuePair<string, List<DateTime>> kvp in _ArQ_Patients_Appt_let_sent_mri)
                {
                    string hospID = kvp.Key;
                    List<DateTime> examDates = kvp.Value;
                    foreach (DateTime examDate in examDates)
                    {
                        if (_CRIS_MRIs.ContainsKey(hospID))
                        {
                            foreach (CRISexam exam in _CRIS_MRIs[hospID])
                            {
                                returnText += hospID + ", , , ," + exam.ExamType.ToString() + ", " + exam.ExamDate.ToString("yyyy-MM-dd");
                                returnText += ", " + exam.Location.ToString() + Environment.NewLine;
                            }
                        }
                    }
                }
            }
            return returnText;
        }


        private string process_CRIS_CANCELS(string hospID, DateTime examDate, out int cancelCnt)
        {
            string cancelsText = string.Empty;
            cancelCnt = 0;
            if (_CRIS_CANCELS.ContainsKey(hospID))
            {

                foreach (CRISexam examCancel in _CRIS_CANCELS[hospID])
                {
                    cancelCnt++;
                    if (!ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                    {
                        cancelsText += hospID + " Cancelled scan - date: " + examDate.ToString("dd:MM:yyyy");
                    }
                    DataRow myRow = _CrisinfoTabCancels.NewRow();
                    string openpage = "mammo";
                    if (string.Compare(_scanCodeToType[examCancel.ExamType], "MRI", true) == 0)
                    {
                        openpage = "scans";
                    }
                    //myRow["  Hospital ID  "] = "<a href=\"" + hospID + "\">" + hospID + "</a>"; 
                    myRow["  Hospital ID  "] = "<a href=\"?uid=" + hospID + "&group=roottable&page=" + openpage + "\">" + hospID + "</a>";
                    myRow["  ArQ Scan Date  "] = examDate;
                    myRow["  Scan Type  "] = _scanCodeToType[examCancel.ExamType];
                    myRow["  Location  "] = examCancel.Location;
                    if (_cancel_codes.ContainsKey(examCancel.Status))
                    {
                        myRow["  Cancel Description  "] = examCancel.Status.ToString() + "-" + _cancel_codes[examCancel.Status];
                        if (!ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                        {
                            cancelsText += " - Type:" + _scanCodeToType[examCancel.ExamType] + " Location:" + examCancel.Location + " Cancel Description:" + examCancel.Status.ToString() + "-" + _cancel_codes[examCancel.Status] + Environment.NewLine;
                        }
                    }
                    else
                    {
                        myRow["  Cancel Description  "] = examCancel.Status;
                        cancelsText += "   -   Location:" + examCancel.Location + " StatusCode:" + examCancel.Status + Environment.NewLine;
                    }
                    _CrisinfoTabCancels.Rows.Add(myRow);
                }
            }
            return cancelsText;
        }

    


        private string setup_CRIS_info_from_csv(int nDaysHistory) //dateIn and nDayHistory in prep for web service call
        {
            // TO BE REPLACED BY GD WEB SERVICE...
            string msg = "";
            string[] lines = System.IO.File.ReadAllLines(ArQ_CFFH_intergration.Properties.Settings.Default.TempCRIS_info_path);
            foreach (string line in lines)
            {

                string[] splits = line.Split(',');
                if (splits.Length == 9)
                {
                    DateTime dateOfScan;
                    DateTime dateReported;
                    if (DateTime.TryParse(splits[5], out dateOfScan)) //Check both dates exist to proceed
                    {
                        if (DateTime.TryParse(splits[6], out dateReported))
                        {
                            CRISexam thisExam = new CRISexam();
                            thisExam.ExamDate = dateOfScan;
                            thisExam.ReportedDate = dateReported;
                            thisExam.HospID = splits[0].ToUpper();
                            thisExam.ExamType = splits[4].ToUpper();
                            thisExam.Status = splits[7].ToUpper();
                            thisExam.Location = splits[8].ToUpper();

                            if (string.Compare(splits[4], "XMAMB", true) == 0)
                            {
                                if (!_CRIS_Mammos.ContainsKey(splits[0].ToUpper()))
                                {
                                    List<CRISexam> newList = new List<CRISexam>();
                                    _CRIS_Mammos.Add(splits[0].ToUpper(), newList);
                                }
                                _CRIS_Mammos[splits[0].ToUpper()].Add(thisExam);
                            }
                            else if ((string.Compare(splits[4].ToUpper(), "MMAMB", true) == 0) || (string.Compare(splits[4].ToUpper(), "MMAMBC", true) == 0))
                            {
                                if (!_CRIS_MRIs.ContainsKey(splits[0].ToUpper()))
                                {
                                    List<CRISexam> newList = new List<CRISexam>();
                                    _CRIS_MRIs.Add(splits[0].ToUpper(), newList);
                                }
                                _CRIS_MRIs[splits[0].ToUpper()].Add(thisExam);
                            }
                        }
                    }
                }
                else
                {
                    msg += "Error- in table not the correct size" + Environment.NewLine;
                }

            }
            return msg;
        }

        private string setup_CRIS_info_from_service(int nDaysHistory, int nDaysBackOffset, bool outWithResults) //dateIn and nDayHistory in prep for web service call
        {
            string msg = "";
            DataTable scansReprted = new DataTable();

            ArQ_CFFH_intergration.GetRISData.BreastScanAppointments webserv = new BreastScanAppointments();
            //ArQ_CFFH_intergration.localhost_debug.BreastScanAppointments webserv = new ArQ_CFFH_intergration.localhost_debug.BreastScanAppointments();

            webserv.Credentials = System.Net.CredentialCache.DefaultCredentials;
            if (outWithResults)
            {
                try
                {
                    scansReprted = webserv.GetBreastScansWithResults(nDaysHistory, nDaysBackOffset);
                }
                catch (Exception Ex)
                {
                    msg += Ex.Message + Environment.NewLine;
                    scansReprted = null;
                }
            }
            else
            {
                try
                {
                    scansReprted = webserv.GetAllBreastScans(nDaysHistory, nDaysBackOffset);
                }
                catch (Exception Ex)
                {
                    msg += Ex.Message + Environment.NewLine;
                    scansReprted = null;
                }
            }

            if (scansReprted != null)
            {
                if (string.Compare(scansReprted.Columns[0].ColumnName, "error", true) == 0)
                {
                    msg = scansReprted.Rows[0]["error"].ToString();
                }
                else
                {
                    for (int i = 0; i < scansReprted.Rows.Count; i++)
                    {

                        DateTime dateOfScan;
                        DateTime dateReported;
                        if (DateTime.TryParse(scansReprted.Rows[i]["EventDate"].ToString(), out dateOfScan)) //Check both dates exist to proceed
                        {
                            if (!DateTime.TryParse(scansReprted.Rows[i]["DateReported"].ToString(), out dateReported))
                            {
                                dateReported = DateTime.MinValue;
                            }

                            CRISexam thisExam = new CRISexam();
                            thisExam.ExamDate = dateOfScan;
                            thisExam.ReportedDate = dateReported;
                            thisExam.HospID = scansReprted.Rows[i]["HospNo"].ToString().ToUpper();
                            thisExam.ExamType = scansReprted.Rows[i]["ExamCode"].ToString().ToUpper();
                            thisExam.Status = scansReprted.Rows[i]["StatusCode"].ToString().ToUpper();
                            thisExam.Location = scansReprted.Rows[i]["RefLocat"].ToString().ToUpper();
                            if ((string.Compare(thisExam.Status.Substring(0, 1), "C", true) != 0) || (!outWithResults))  // if this is not a cancelled/dna etc appt...
                            {
                                if (string.Compare(thisExam.ExamType, "XMAMB", true) == 0)
                                {
                                    if (!_CRIS_Mammos.ContainsKey(thisExam.HospID))
                                    {
                                        List<CRISexam> newList = new List<CRISexam>();
                                        _CRIS_Mammos.Add(thisExam.HospID, newList);
                                    }
                                    _CRIS_Mammos[thisExam.HospID].Add(thisExam);
                                }
                                else if ((string.Compare(thisExam.ExamType, "MMAMB", true) == 0) || (string.Compare(thisExam.ExamType, "MMAMBC", true) == 0))
                                {
                                    if (!_CRIS_MRIs.ContainsKey(thisExam.HospID))
                                    {
                                        List<CRISexam> newList = new List<CRISexam>();
                                        _CRIS_MRIs.Add(thisExam.HospID, newList);
                                    }
                                    _CRIS_MRIs[thisExam.HospID].Add(thisExam);
                                }
                            }
                            else
                            {
                                if (!_CRIS_CANCELS.ContainsKey(thisExam.HospID))
                                {
                                    List<CRISexam> newList = new List<CRISexam>();
                                    _CRIS_CANCELS.Add(thisExam.HospID, newList);
                                }
                                _CRIS_CANCELS[thisExam.HospID].Add(thisExam);
                            }
                        }
                    }
                    List<string> deleteCancels = new List<string>();
                    foreach (KeyValuePair<string, List<CRISexam>> kvp in _CRIS_CANCELS)
                    {
                        if((_CRIS_Mammos.ContainsKey(kvp.Key))||(_CRIS_MRIs.ContainsKey(kvp.Key)))
                        {
                            deleteCancels.Add(kvp.Key);
                        }
                    }
                    foreach (string delID in deleteCancels)
                    {
                        _CRIS_CANCELS.Remove(delID);
                    }
                }

            }
            else
            {
                msg += "Null Table returned from webservice call";
                //table returned was null
            }
            return msg;
        }

        private string setupArqApptLetterSentMammo()
        {
            string msg = "";
            try
            {
                SqlConnection connection = new SqlConnection(ArQ_CFFH_intergration.Properties.Settings.Default.CFFH_connection_string);
                string sql_string = "SELECT roottable.[demog$hospital_id], mammo.[scan_date_mammo$date]";
                sql_string += " FROM mammo INNER JOIN roottable ON mammo.parentkey = roottable.primarykey";
                sql_string += " WHERE (((mammo.[status_mammo$item])=8) AND ((roottable.deleted)<>1) AND ((mammo.deleted)<>1) AND ((roottable.[ref_status$item])=0));";
                SqlDataAdapter adapter = new SqlDataAdapter(sql_string, connection);
                DataTable allPtsWithApptLetSent = new DataTable();
                adapter.Fill(allPtsWithApptLetSent);

                for (int i = 0; i < allPtsWithApptLetSent.Rows.Count; i++)
                {
                    string thisHospId = allPtsWithApptLetSent.Rows[i]["demog$hospital_id"].ToString().ToUpper();
                    DateTime thisApptDate = DateTime.Parse(allPtsWithApptLetSent.Rows[i]["scan_date_mammo$date"].ToString());

                    if (!_ArQ_Patients_Appt_let_sent_mammo.ContainsKey(thisHospId))
                    {
                        List<DateTime> newList = new List<DateTime>();
                        _ArQ_Patients_Appt_let_sent_mammo.Add(thisHospId, newList);
                    }
                    _ArQ_Patients_Appt_let_sent_mammo[thisHospId].Add(thisApptDate);
                }
            }
            catch (Exception ex1)
            {
                msg = "Exception_Appt_let_sent_Mammo: " + ex1.Message;
            }
            return msg;
        }

        private string setupArqApptLetterSentMRI()
        {
            string msg = "";
            try
            {
                SqlConnection connection = new SqlConnection(ArQ_CFFH_intergration.Properties.Settings.Default.CFFH_connection_string);
                string sql_string = "SELECT roottable.[demog$hospital_id], mri.[scan_date_mri$date]";
                sql_string += " FROM mri INNER JOIN roottable ON mri.parentkey = roottable.primarykey";
                sql_string += " WHERE (((mri.[status_mri$item])=8) AND ((roottable.deleted)<>1) AND ((mri.deleted)<>1) AND ((roottable.[ref_status$item])=0));";
                SqlDataAdapter adapter = new SqlDataAdapter(sql_string, connection);
                DataTable allPtsWithApptLetSent = new DataTable();
                adapter.Fill(allPtsWithApptLetSent);

                for (int i = 0; i < allPtsWithApptLetSent.Rows.Count; i++)
                {
                    string thisHospId = allPtsWithApptLetSent.Rows[i]["demog$hospital_id"].ToString().ToUpper();
                    DateTime thisApptDate = DateTime.Parse(allPtsWithApptLetSent.Rows[i]["scan_date_mri$date"].ToString());

                    if (!_ArQ_Patients_Appt_let_sent_mri.ContainsKey(thisHospId))
                    {
                        List<DateTime> newList = new List<DateTime>();
                        _ArQ_Patients_Appt_let_sent_mri.Add(thisHospId, newList);
                    }
                    _ArQ_Patients_Appt_let_sent_mri[thisHospId].Add(thisApptDate);
                }
            }
            catch (Exception ex1)
            {
                msg = "Exception_Appt_let_sent_MRI: " + ex1.Message;
            }
            return msg;
        }

        private string setupArqAllCurrent()
        {
            string msg = "";
            try
            {
                SqlConnection connection = new SqlConnection(ArQ_CFFH_intergration.Properties.Settings.Default.CFFH_connection_string);
                string sql_string = "SELECT roottable.[demog$hospital_id] FROM roottable WHERE ((roottable.[ref_status$item]=0) AND (roottable.deleted<>1));";
                SqlDataAdapter adapter = new SqlDataAdapter(sql_string, connection);
                DataTable allCurrentPatients = new DataTable();
                adapter.Fill(allCurrentPatients);

                for (int i = 0; i < allCurrentPatients.Rows.Count; i++)
                {
                    _ArQ_Patients_All_Current.Add(allCurrentPatients.Rows[i]["demog$hospital_id"].ToString().ToUpper());
                }
            }
            catch (Exception ex1)
            {
                msg = "Exception_query_all_patients: " + ex1.Message;
            }
            return msg;
        }

        private string setupArqAllCurrentWitoutTimeWinAPPt(int ndays,int offset)
        {
            DateTime endDateWindow = DateTime.Now;
            endDateWindow = endDateWindow.AddDays(-offset);
            DateTime startDateWindow = endDateWindow.AddDays(-ndays);

            string msg = "";
            try
            {
                SqlConnection connection = new SqlConnection(ArQ_CFFH_intergration.Properties.Settings.Default.CFFH_connection_string);
                string sql_string = "SELECT DISTINCT roottable.[demog$hospital_id] FROM roottable";
                sql_string += " WHERE (";
                sql_string += " (roottable.[ref_status$item]=0) AND (roottable.deleted=0) ";

                sql_string += " AND (roottable.[demog$hospital_id] not in (";
                sql_string += " SELECT  roottable.[demog$hospital_id]";
                sql_string += " FROM mammo INNER JOIN roottable ON mammo.parentkey = roottable.primarykey";
                sql_string += " WHERE ((mammo.[scan_date_mammo$date] >='" + startDateWindow.ToString("yyyy-MM-dd") + "') ";
                sql_string += " AND  (mammo.[scan_date_mammo$date] <='" + endDateWindow.ToString("yyyy-MM-dd") + "') ) ) ) "; 

                sql_string += " AND (roottable.[demog$hospital_id] not in (";
                sql_string += " SELECT  roottable.[demog$hospital_id]";
                sql_string += " FROM mri INNER JOIN roottable ON mri.parentkey = roottable.primarykey";
                sql_string += " WHERE ((mri.[scan_date_mri$date] >='" + startDateWindow.ToString("yyyy-MM-dd") + "') ";
                sql_string += " AND  (mri.[scan_date_mri$date] <='" + endDateWindow.ToString("yyyy-MM-dd") + "') ) ) ) ";
                sql_string += " )";
               
                SqlDataAdapter adapter = new SqlDataAdapter(sql_string, connection);
                DataTable allCurrentPatients = new DataTable();
                adapter.Fill(allCurrentPatients);

                for (int i = 0; i < allCurrentPatients.Rows.Count; i++)
                {
                    _ArQ_Patients_All_Current.Add(allCurrentPatients.Rows[i]["demog$hospital_id"].ToString().ToUpper());
                }
            }
            catch (Exception ex1)
            {
                msg = "Exception_query_all_patients: " + ex1.Message;
            }
            return msg;

        }




        private string Get_audit_querys()
        {
            string returnString = "";
            string queryErrors = "";
            string hospID;


            DataTable missingActiveMammo = QueryCurrentButMissingActiveApptMammo(out queryErrors); //has future appts (pending) but no currently active appt (waiting recall or Appt letter sent)
            returnString += "<hr/>** Serious Issue ** (Stalled mammo plans) <br/>  - Current patients with future mammo appts but no current active appointment" + Environment.NewLine + Environment.NewLine;
            if (missingActiveMammo.Rows.Count > 0)
            {
                for (int i = 0; i < missingActiveMammo.Rows.Count; i++)
                {
                    if (!ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                    {
                        returnString += missingActiveMammo.Rows[i]["demog$hospital_id"] + " - (" + missingActiveMammo.Rows[i]["demog$forename"] + " ";
                        returnString += missingActiveMammo.Rows[i]["demog$surname"] + " DOB:" + DateTime.Parse(missingActiveMammo.Rows[i]["demog$dob"].ToString()).ToString("dd-MMM-yyyy") + ")" + Environment.NewLine;

                    }
                    DataRow myrow = _qu_missingActiveMammo.NewRow();
                    hospID = missingActiveMammo.Rows[i]["demog$hospital_id"].ToString();
                    myrow["  Hospital ID  "] = "<a href=\"?uid=" + hospID + "&group=roottable&page=mammo\">" + hospID + "</a>";
                    //myrow["  Hospital ID  "] = "<a href=\"" + hospID + "\">" + hospID + "</a>";
                    myrow["  Forename  "] = missingActiveMammo.Rows[i]["demog$forename"];
                    myrow["  Surname  "] = missingActiveMammo.Rows[i]["demog$surname"];
                    myrow["  DOB  "] = DateTime.Parse(missingActiveMammo.Rows[i]["demog$dob"].ToString());
                    _qu_missingActiveMammo.Rows.Add(myrow);
                }
                if (ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                {
                    DataTableHelper DTHmissingActiveMammo = new DataTableHelper();
                    returnString += DTHmissingActiveMammo.DataTableToHtmlTable(_qu_missingActiveMammo);
                }
                returnString += Environment.NewLine + Environment.NewLine;
            }
            else
            {
                returnString += "      - No patients found -" + Environment.NewLine + Environment.NewLine;
            }
            _errors.AppendLine(queryErrors + Environment.NewLine);

            DataTable missingActiveMRI = QueryCurrentButMissingActiveApptMRI(out queryErrors); //has future appts (pending) but no currently active appt (waiting recall or Appt letter sent)
            returnString += "<hr/>** Serious Issue ** (Stalled MRI plans)<br/>  - Current patients with future MRI appts but no current active appointment" + Environment.NewLine + Environment.NewLine;
            if (missingActiveMRI.Rows.Count > 0)
            {
                for (int i = 0; i < missingActiveMRI.Rows.Count; i++)
                {
                    if (!ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                    {
                        //returnString += missingActiveMRI.Rows[i]["demog$hospital_id"] + "," + Environment.NewLine;
                        returnString += missingActiveMRI.Rows[i]["demog$hospital_id"] + " - (" + missingActiveMRI.Rows[i]["demog$forename"] + " ";
                        returnString += missingActiveMRI.Rows[i]["demog$surname"] + " DOB:" + DateTime.Parse(missingActiveMRI.Rows[i]["demog$dob"].ToString()).ToString("dd-MMM-yyyy") + ")" + Environment.NewLine;

                    }
                    DataRow myrow = _qu_missingActiveMRI.NewRow();
                    hospID =missingActiveMRI.Rows[i]["demog$hospital_id"].ToString();
                    myrow["  Hospital ID  "] = "<a href=\"?uid=" + hospID + "&group=roottable&page=scans\">" + hospID + "</a>"; //"<a href=\"" + hospID + "\">" + hospID + "</a>";
                    myrow["  Forename  "] = missingActiveMRI.Rows[i]["demog$forename"];
                    myrow["  Surname  "] = missingActiveMRI.Rows[i]["demog$surname"];
                    myrow["  DOB  "] = DateTime.Parse(missingActiveMRI.Rows[i]["demog$dob"].ToString());
                    _qu_missingActiveMRI.Rows.Add(myrow);

                }
                if (ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                {
                    DataTableHelper DTHmissingActiveMRI = new DataTableHelper();
                    returnString += DTHmissingActiveMRI.DataTableToHtmlTable(_qu_missingActiveMRI);
                }
                returnString += Environment.NewLine + Environment.NewLine;
            }
            else
            {
                returnString += "      - No patients found -" + Environment.NewLine + Environment.NewLine;
            }
            _errors.AppendLine(queryErrors + Environment.NewLine);

            DataTable deceasedCurrent = QueryCurrentButDead(out queryErrors);
            returnString += "<hr/>** Serious Issue ** (Deceased patients)<br/>  - Current patients who are now recorded as deceased in Lorenzo - Need to discontinue" + Environment.NewLine + Environment.NewLine;

            if (deceasedCurrent.Rows.Count > 0)
            {
                for (int i = 0; i < deceasedCurrent.Rows.Count; i++)
                {
                    if (!ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                    {
                        //returnString += deceasedCurrent.Rows[i]["demog$hospital_id"] + "," + Environment.NewLine;
                        returnString += deceasedCurrent.Rows[i]["demog$hospital_id"] + " - (" + deceasedCurrent.Rows[i]["demog$forename"] + " ";
                        returnString += deceasedCurrent.Rows[i]["demog$surname"] + " DOB:" + DateTime.Parse(deceasedCurrent.Rows[i]["demog$dob"].ToString()).ToString("dd-MMM-yyyy") + ")" + Environment.NewLine;
                      
                    }
                    DataRow myrow = _qu_deceasedCurrent.NewRow();
                    hospID=deceasedCurrent.Rows[i]["demog$hospital_id"].ToString();
                    myrow["  Hospital ID  "] = "<a href=\"?uid=" + hospID + "&group=roottable&page=scans\">" + hospID + "</a>"; //=  "<a href=\"" + hospID + "\">" + hospID + "</a>";
                    myrow["  Forename  "] = deceasedCurrent.Rows[i]["demog$forename"];
                    myrow["  Surname  "] = deceasedCurrent.Rows[i]["demog$surname"];
                    myrow["  DOB  "] = DateTime.Parse(deceasedCurrent.Rows[i]["demog$dob"].ToString());
                    _qu_deceasedCurrent.Rows.Add(myrow);

                }
                if (ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                {
                    DataTableHelper DTHdeceasedCurrent = new DataTableHelper();
                    returnString += DTHdeceasedCurrent.DataTableToHtmlTable(_qu_deceasedCurrent);
                }
                returnString += Environment.NewLine + Environment.NewLine;
            }
            else
            {
                returnString += "      - No patients found -" + Environment.NewLine + Environment.NewLine;
            }
            _errors.AppendLine(queryErrors + Environment.NewLine);


            DataTable missingWRMammoCurrent = QueryCurrentButNoWaitingRecallMammo(out queryErrors);
            returnString += "<hr/>** Warning Issue ** (Missing Waiting Recall Mammo)<br/>  - Current Mammo patients who do not have a Waiting Recall status appointment " + Environment.NewLine + Environment.NewLine;

            if (missingWRMammoCurrent.Rows.Count > 0)
            {
                for (int i = 0; i < missingWRMammoCurrent.Rows.Count; i++)
                {
                    if (!ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                    {
                        //returnString += missingWRMammoCurrent.Rows[i]["demog$hospital_id"] + "," + Environment.NewLine;
                        returnString += missingWRMammoCurrent.Rows[i]["demog$hospital_id"] + " - (" + missingWRMammoCurrent.Rows[i]["demog$forename"] + " ";
                        returnString += missingWRMammoCurrent.Rows[i]["demog$surname"] + " DOB:" + DateTime.Parse(missingWRMammoCurrent.Rows[i]["demog$dob"].ToString()).ToString("dd-MMM-yyyy") + ")" + Environment.NewLine;

                    }
                    DataRow myrow = _qu_missingWRMammoCurrent.NewRow();
                    hospID= missingWRMammoCurrent.Rows[i]["demog$hospital_id"].ToString();
                    myrow["  Hospital ID  "] = "<a href=\"?uid=" + hospID + "&group=roottable&page=mammo\">" + hospID + "</a>";  //="<a href=\"" + hospID + "\">" + hospID + "</a>";
                    myrow["  Forename  "] = missingWRMammoCurrent.Rows[i]["demog$forename"];
                    myrow["  Surname  "] = missingWRMammoCurrent.Rows[i]["demog$surname"];
                    myrow["  DOB  "] = DateTime.Parse(missingWRMammoCurrent.Rows[i]["demog$dob"].ToString());
                    _qu_missingWRMammoCurrent.Rows.Add(myrow);

                }

                if (ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                {
                    DataTableHelper DTHmissingWRMammoCurrent = new DataTableHelper();
                    returnString += DTHmissingWRMammoCurrent.DataTableToHtmlTable(_qu_missingWRMammoCurrent);
                }
                returnString += Environment.NewLine + Environment.NewLine;
            }
            else
            {
                returnString += "      - No patients found -" + Environment.NewLine + Environment.NewLine;
            }
            _errors.AppendLine(queryErrors + Environment.NewLine);

            DataTable OldWRMammoCurrent = QueryCurrentButWaitingRecallOld(out queryErrors);
            returnString += "<hr/>** Warning Issue ** (Waiting Recall very overdue)<br/>  - Current Mammo patients who have a Waiting Recall status appointment over 3 months overdue " + Environment.NewLine + Environment.NewLine;

            if (OldWRMammoCurrent.Rows.Count > 0)
            {
                for (int i = 0; i < OldWRMammoCurrent.Rows.Count; i++)
                {
                    if (!ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                    {
                        //returnString += missingWRMammoCurrent.Rows[i]["demog$hospital_id"] + "," + Environment.NewLine;
                        returnString += OldWRMammoCurrent.Rows[i]["demog$hospital_id"] + " - (" + OldWRMammoCurrent.Rows[i]["demog$forename"] + " ";
                        returnString += OldWRMammoCurrent.Rows[i]["demog$surname"] + " DOB:" + DateTime.Parse(OldWRMammoCurrent.Rows[i]["demog$dob"].ToString()).ToString("dd-MMM-yyyy") + ")" + Environment.NewLine;
                    }
                    DataRow myrow = _qu_OldWRMammoCurrent.NewRow();
                    hospID= OldWRMammoCurrent.Rows[i]["demog$hospital_id"].ToString();
                    myrow["  Hospital ID  "] = "<a href=\"?uid=" + hospID + "&group=roottable&page=mammo\">" + hospID + "</a>"; // = "<a href=\"" + hospID + "\">" + hospID + "</a>";;
                    myrow["  Forename  "] = OldWRMammoCurrent.Rows[i]["demog$forename"];
                    myrow["  Surname  "] = OldWRMammoCurrent.Rows[i]["demog$surname"];
                    myrow["  DOB  "] = DateTime.Parse(OldWRMammoCurrent.Rows[i]["demog$dob"].ToString());
                    _qu_OldWRMammoCurrent.Rows.Add(myrow);
                }
                if (ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                {
                    DataTableHelper DTHOldWRMammoCurrent = new DataTableHelper();
                    returnString += DTHOldWRMammoCurrent.DataTableToHtmlTable(_qu_OldWRMammoCurrent);
                }
                returnString += Environment.NewLine + Environment.NewLine;
            }
            else
            {
                returnString += "      - No patients found -" + Environment.NewLine + Environment.NewLine;
            }

            DataTable NoMammoApptsOrProtocol = QueryNoMammoApptsOrProtocol(out queryErrors);
            returnString += "<hr/>** Warning Issue ** (No Mammo Appointments or Protocol defined)<br/>  - This indicates the patient may not have a working plan defined" + Environment.NewLine + Environment.NewLine;

            if (NoMammoApptsOrProtocol.Rows.Count > 0)
            {
                for (int i = 0; i < NoMammoApptsOrProtocol.Rows.Count; i++)
                {
                    if (!ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                    {
                        returnString += NoMammoApptsOrProtocol.Rows[i]["demog$hospital_id"] + " - (" + NoMammoApptsOrProtocol.Rows[i]["demog$forename"] + " ";
                        returnString += NoMammoApptsOrProtocol.Rows[i]["demog$surname"] + " DOB:" + DateTime.Parse(NoMammoApptsOrProtocol.Rows[i]["demog$dob"].ToString()).ToString("dd-MMM-yyyy") + ")" + Environment.NewLine;
                    }
                    DataRow myrow = _qu_noMammosOrProtocol.NewRow();
                    hospID = NoMammoApptsOrProtocol.Rows[i]["demog$hospital_id"].ToString();
                    myrow["  Hospital ID  "] = "<a href=\"?uid=" + hospID + "&group=roottable&page=mammo\">" + hospID + "</a>"; // = "<a href=\"" + hospID + "\">" + hospID + "</a>"; ;
                    myrow["  Forename  "] = NoMammoApptsOrProtocol.Rows[i]["demog$forename"];
                    myrow["  Surname  "] = NoMammoApptsOrProtocol.Rows[i]["demog$surname"];
                    myrow["  DOB  "] = DateTime.Parse(NoMammoApptsOrProtocol.Rows[i]["demog$dob"].ToString());
                    _qu_noMammosOrProtocol.Rows.Add(myrow);
                }
                if (ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                {
                    DataTableHelper DTHNoMammoApptsOrProtocol = new DataTableHelper();
                    returnString += DTHNoMammoApptsOrProtocol.DataTableToHtmlTable(_qu_noMammosOrProtocol);
                }
                returnString += Environment.NewLine + Environment.NewLine;
            }
            else
            {
                returnString += "      - No patients found -" + Environment.NewLine + Environment.NewLine;
            }

            DataTable CurrentButDischargeMammo = QueryCurrentButWithEndApptMammo(out queryErrors);
            returnString += "<hr/>** Warning Issue ** (Current but with a mammo discharged/discontinue appointment)<br/>  - This indicates the patient may need making non-current" + Environment.NewLine + Environment.NewLine;

            if (CurrentButDischargeMammo.Rows.Count > 0)
            {
                for (int i = 0; i < CurrentButDischargeMammo.Rows.Count; i++)
                {
                    if (!ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                    {
                        returnString += CurrentButDischargeMammo.Rows[i]["demog$hospital_id"] + " - (" + CurrentButDischargeMammo.Rows[i]["demog$forename"] + " ";
                        returnString += CurrentButDischargeMammo.Rows[i]["demog$surname"] + " DOB:" + DateTime.Parse(CurrentButDischargeMammo.Rows[i]["demog$dob"].ToString()).ToString("dd-MMM-yyyy") + ")" + Environment.NewLine;
                    }
                    DataRow myrow = _qu_CurrentButWithEndApptMammo.NewRow();
                    hospID = CurrentButDischargeMammo.Rows[i]["demog$hospital_id"].ToString();
                    myrow["  Hospital ID  "] = "<a href=\"?uid=" + hospID + "&group=roottable&page=mammo\">" + hospID + "</a>"; // = "<a href=\"" + hospID + "\">" + hospID + "</a>"; ;
                    myrow["  Forename  "] = CurrentButDischargeMammo.Rows[i]["demog$forename"];
                    myrow["  Surname  "] = CurrentButDischargeMammo.Rows[i]["demog$surname"];
                    myrow["  DOB  "] = DateTime.Parse(CurrentButDischargeMammo.Rows[i]["demog$dob"].ToString());
                    _qu_CurrentButWithEndApptMammo.Rows.Add(myrow);
                }
                if (ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                {
                    DataTableHelper DTHCurrentButDischargeMammo = new DataTableHelper();
                    returnString += DTHCurrentButDischargeMammo.DataTableToHtmlTable(_qu_CurrentButWithEndApptMammo);
                }
                returnString += Environment.NewLine + Environment.NewLine;
            }
            else
            {
                returnString += "      - No patients found -" + Environment.NewLine + Environment.NewLine;
            }

            DataTable NonCurrentButNoDischargeAndFutureApptsMammo = QueryNonCurrentButNoDischargeAndFutureApptsMammo(out queryErrors);
            returnString += "<hr/>** Warning Issue ** (Not Current but no discharged/discontinue mammo appointment and future apps exist)<br/>  - This could indicates accidental end OR future appontments not ended (needs tidying)" + Environment.NewLine + Environment.NewLine;

            if (NonCurrentButNoDischargeAndFutureApptsMammo.Rows.Count > 0)
            {
                for (int i = 0; i < NonCurrentButNoDischargeAndFutureApptsMammo.Rows.Count; i++)
                {
                    if (!ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                    {
                        returnString += NonCurrentButNoDischargeAndFutureApptsMammo.Rows[i]["demog$hospital_id"] + " - (" + NonCurrentButNoDischargeAndFutureApptsMammo.Rows[i]["demog$forename"] + " ";
                        returnString += NonCurrentButNoDischargeAndFutureApptsMammo.Rows[i]["demog$surname"] + " DOB:" + DateTime.Parse(NonCurrentButNoDischargeAndFutureApptsMammo.Rows[i]["demog$dob"].ToString()).ToString("dd-MMM-yyyy") + ")" + Environment.NewLine;
                    }
                    DataRow myrow = _qu_QueryNonCurrentButNoDischargeAndFutureApptsMammo.NewRow();
                    hospID = NonCurrentButNoDischargeAndFutureApptsMammo.Rows[i]["demog$hospital_id"].ToString();
                    myrow["  Hospital ID  "] = "<a href=\"?uid=" + hospID + "&group=roottable&page=mammo\">" + hospID + "</a>"; // = "<a href=\"" + hospID + "\">" + hospID + "</a>"; ;
                    myrow["  Forename  "] = NonCurrentButNoDischargeAndFutureApptsMammo.Rows[i]["demog$forename"];
                    myrow["  Surname  "] = NonCurrentButNoDischargeAndFutureApptsMammo.Rows[i]["demog$surname"];
                    myrow["  DOB  "] = DateTime.Parse(NonCurrentButNoDischargeAndFutureApptsMammo.Rows[i]["demog$dob"].ToString());
                    _qu_QueryNonCurrentButNoDischargeAndFutureApptsMammo.Rows.Add(myrow);
                }
                if (ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                {
                    DataTableHelper DTHNonCurrentButNoDischargeAndFutureApptsMammo = new DataTableHelper();
                    returnString += DTHNonCurrentButNoDischargeAndFutureApptsMammo.DataTableToHtmlTable(_qu_QueryNonCurrentButNoDischargeAndFutureApptsMammo);
                }
                returnString += Environment.NewLine + Environment.NewLine;
            }
            else
            {
                returnString += "      - No patients found -" + Environment.NewLine + Environment.NewLine;
            }

            DataTable RecentlyEnded = QueryRecentlyEnded(out queryErrors);
            returnString += "<hr/>** Audit List ** Discontinued/Discharged patients in the last Month (from 5/feb/2018)" + Environment.NewLine + Environment.NewLine;

            if (RecentlyEnded.Rows.Count > 0)
            {
                for (int i = 0; i < RecentlyEnded.Rows.Count; i++)
                {
                    if (!ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                    {
                        returnString += RecentlyEnded.Rows[i]["demog$hospital_id"] + " - (" + RecentlyEnded.Rows[i]["demog$forename"] + " ";
                        returnString += RecentlyEnded.Rows[i]["demog$surname"] + " DOB:" + DateTime.Parse(RecentlyEnded.Rows[i]["demog$dob"].ToString()).ToString("dd-MMM-yyyy") + ")" + Environment.NewLine;
                    }
                    DataRow myrow = _qu_RecentlyEnded.NewRow();
                    hospID = RecentlyEnded.Rows[i]["demog$hospital_id"].ToString();
                    myrow["  Hospital ID  "] = "<a href=\"?uid=" + hospID + "&group=roottable&page=mammo\">" + hospID + "</a>"; // = "<a href=\"" + hospID + "\">" + hospID + "</a>"; ;
                    myrow["  Forename  "] = RecentlyEnded.Rows[i]["demog$forename"];
                    myrow["  Surname  "] = RecentlyEnded.Rows[i]["demog$surname"];
                    myrow["  DOB  "] = DateTime.Parse(RecentlyEnded.Rows[i]["demog$dob"].ToString());
                    if((int)RecentlyEnded.Rows[i]["ref_status$item"]==1)
                    {
                        myrow["  End Type  "] ="Discontinued";
                        myrow["  Date Ended  "] = DateTime.Parse(RecentlyEnded.Rows[i]["ref_discontinued$date"].ToString());
                    }
                    else
                    {
                         myrow["  End Type  "] ="Discharged";
                         myrow["  Date Ended  "] = DateTime.Parse(RecentlyEnded.Rows[i]["ref_discharged$date"].ToString());
                    }

                    _qu_RecentlyEnded.Rows.Add(myrow);
                }
                if (ArQ_CFFH_intergration.Properties.Settings.Default.useTabelsOutput)
                {
                    DataTableHelper DTHRecentlyEnded = new DataTableHelper();
                    returnString += DTHRecentlyEnded.DataTableToHtmlTable(_qu_RecentlyEnded);
                }
                returnString += Environment.NewLine + Environment.NewLine;
            }
            else
            {
                returnString += "      - No patients found -" + Environment.NewLine + Environment.NewLine;
            }



            _errors.AppendLine(queryErrors + Environment.NewLine);
            returnString += "<hr/>";
            return returnString;
        }

        private DataTable QueryCurrentButMissingActiveApptMammo(out string errormsg)
        {
            errormsg = "";

            string sql_string = "SELECT DISTINCT roottable.[demog$hospital_id],roottable.[demog$surname],roottable.[demog$forename],roottable.[demog$dob] FROM roottable";
            sql_string += " WHERE ( ( (roottable.[ref_status$item])=0) AND ((roottable.deleted)=0) AND (roottable.[demog$hospital_id] not in (";
            sql_string += " SELECT  roottable.[demog$hospital_id]";
            sql_string += " FROM mammo INNER JOIN roottable ON mammo.parentkey = roottable.primarykey";
            sql_string += " WHERE ( ( (roottable.[ref_status$item])=0)  AND (roottable.deleted=0)  AND (mammo.deleted=0) ";
            sql_string += " AND ((mammo.[status_mammo$item]=7) OR (mammo.[status_mammo$item]=8)) ) ) ) ";
            sql_string += " AND (roottable.[demog$hospital_id] in (";
            sql_string += " SELECT  roottable.[demog$hospital_id]";
            sql_string += " FROM mammo INNER JOIN roottable ON mammo.parentkey = roottable.primarykey ";
            sql_string += " WHERE ( ( (roottable.[ref_status$item])=0) AND ((roottable.deleted)=0) AND ((mammo.[status_mammo$item])=6)  ";
            sql_string += " AND ((mammo.deleted)=0) ) ) ) );";
            DataTable Result = QueryRunSelect(sql_string, "Error-Query Current But No 'WaitingRecall' or 'Appt letter sent'", out errormsg);
            return Result;

        }

        private DataTable QueryCurrentButMissingActiveApptMRI(out string errormsg)
        {
            errormsg = "";

            string sql_string = "SELECT DISTINCT roottable.[demog$hospital_id],roottable.[demog$surname],roottable.[demog$forename],roottable.[demog$dob] FROM roottable";
            sql_string += " WHERE ( ( (roottable.[ref_status$item])=0) AND ((roottable.deleted)=0) AND (roottable.[demog$hospital_id] not in (";
            sql_string += " SELECT  roottable.[demog$hospital_id]";
            sql_string += " FROM mri INNER JOIN roottable ON mri.parentkey = roottable.primarykey";
            sql_string += " WHERE ( ( (roottable.[ref_status$item])=0)  AND (roottable.deleted=0)  AND (mri.deleted=0) ";
            sql_string += " AND ((mri.[status_mri$item]=7) OR (mri.[status_mri$item]=8)) ) ) ) "; //not got any waiting or appt letter sent
            sql_string += " AND (roottable.[demog$hospital_id] in (";
            sql_string += " SELECT  roottable.[demog$hospital_id]";
            sql_string += " FROM mri INNER JOIN roottable ON mri.parentkey = roottable.primarykey ";
            sql_string += " WHERE ( ( (roottable.[ref_status$item])=0) AND ((roottable.deleted)=0) AND (  ((mri.[status_mri$item])=6) OR ((mri.[status_mri$item])=11)  ) ";
            sql_string += " AND ((mri.deleted)=0) ) ) ) );"; // has at least 1 pending or provisional
            DataTable Result = QueryRunSelect(sql_string, "Error-Query Current But No 'WaitingRecall' or 'Appt letter sent'", out errormsg);
            return Result;

        }

        private DataTable QueryCurrentButNoWaitingRecallMammo(out string errormsg)
        {
            errormsg = "";


            string sql_string = "SELECT DISTINCT roottable.[demog$hospital_id],roottable.[demog$surname],roottable.[demog$forename],roottable.[demog$dob] FROM roottable";
            sql_string += " WHERE ( ( (roottable.[ref_status$item])=0) AND ((roottable.deleted)=0) AND (roottable.[demog$hospital_id] not in (";
            sql_string += " SELECT  roottable.[demog$hospital_id]";
            sql_string += " FROM mammo INNER JOIN roottable ON mammo.parentkey = roottable.primarykey";
            sql_string += " WHERE ( ( (roottable.[ref_status$item])=0)  AND (roottable.deleted=0)  AND (mammo.deleted=0) ";
            sql_string += " AND (mammo.[status_mammo$item]=7) ) ) ) ";
            sql_string += " AND (roottable.[demog$hospital_id] in (";
            sql_string += " SELECT  roottable.[demog$hospital_id]";
            sql_string += " FROM mammo INNER JOIN roottable ON mammo.parentkey = roottable.primarykey ";
            sql_string += " WHERE ( ( (roottable.[ref_status$item])=0) AND ((roottable.deleted)=0) AND ((mammo.[status_mammo$item])=6)  ";
            sql_string += " AND ((mammo.deleted)=0) ) ) ) );";
            DataTable Result = QueryRunSelect(sql_string, "Error-Query Current But No 'WaitingRecall'", out errormsg);

            return Result;
        }

        private DataTable QueryCurrentButNoWaitingRecallMRI(out string errormsg)
        {
            errormsg = "";


            string sql_string = "SELECT DISTINCT roottable.[demog$hospital_id],roottable.[demog$surname],roottable.[demog$forename],roottable.[demog$dob] FROM roottable";
            sql_string += " WHERE ( ( (roottable.[ref_status$item])=0) AND ((roottable.deleted)=0) AND (roottable.[demog$hospital_id] not in (";
            sql_string += " SELECT  roottable.[demog$hospital_id]";
            sql_string += " FROM mri INNER JOIN roottable ON mri.parentkey = roottable.primarykey";
            sql_string += " WHERE ( ( (roottable.[ref_status$item])=0)  AND (roottable.deleted=0)  AND (mri.deleted=0) ";
            sql_string += " AND (mri.[status_mri$item]=7) ) ) ) ";
            sql_string += " AND (roottable.[demog$hospital_id] in (";
            sql_string += " SELECT  roottable.[demog$hospital_id]";
            sql_string += " FROM mammo INNER JOIN roottable ON mammo.parentkey = roottable.primarykey ";
            sql_string += " WHERE ( ( (roottable.[ref_status$item])=0) AND ((roottable.deleted)=0) AND ((mammo.[status_mammo$item])=6)  ";
            sql_string += " AND ((mammo.deleted)=0) ) ) ) );";
            DataTable Result = QueryRunSelect(sql_string, "Error-Query Current But No 'WaitingRecall'", out errormsg);

            return Result;

        }

        private DataTable QueryCurrentButWaitingRecallOld(out string errormsg)
        {
            errormsg = "";
            DateTime twoMonthsAgo = DateTime.Now.AddMonths(-2);

            string sql_string = " SELECT DISTINCT roottable.[demog$hospital_id],roottable.[demog$surname],roottable.[demog$forename],roottable.[demog$dob] ";
            sql_string += " FROM mammo INNER JOIN roottable ON mammo.parentkey = roottable.primarykey";
            sql_string += " WHERE ( ( (roottable.[ref_status$item])=0)  AND (roottable.deleted=0)  AND (mammo.deleted=0) AND (mammo.[status_mammo$item]=7) ";
            sql_string += " AND (mammo.scan_date_mammo$date < '" + twoMonthsAgo.ToString("yyyy-MM-dd") +"' ) ) ";

            DataTable Result = QueryRunSelect(sql_string, "Error-Query Current But 'WaitingRecall' > 1 month", out errormsg);

            return Result;

        }

        private DataTable QueryCurrentButDead(out string errormsg)
        {
            errormsg = "";
            string sql_string = "SELECT roottable.[demog$hospital_id],roottable.[demog$surname],roottable.[demog$forename],roottable.[demog$dob],roottable.[demog$deceased], roottable.[ref_status$item]";
            sql_string += " FROM roottable WHERE (((roottable.[demog$deceased])=1) AND ((roottable.[ref_status$item])=0 Or (roottable.[ref_status$item]) Is Null));";
            DataTable Result = QueryRunSelect(sql_string, "Error-Query Current But Deceased", out errormsg);
            return Result;
        }

        private DataTable QueryNoMammoApptsOrProtocol(out string errormsg)
        {
            errormsg = "";
            string sql_string = " SELECT DISTINCT rowChecker.demog$hospital_id, rowChecker.demog$forename, rowChecker.demog$surname,rowChecker.demog$dob from ";
            sql_string += " (SELECT roottable.demog$hospital_id, mammo.parentkey, roottable.[demog$surname],roottable.[demog$forename],roottable.[demog$dob],roottable.ref_referral_plan$item";
            sql_string += "   FROM [breast_scanning].[dbo].[roottable] ";
            sql_string += "   Left outer join [breast_scanning].[dbo].[mammo] on mammo.parentkey=roottable.primarykey";
            sql_string += "   where roottable.ref_status$item=0 AND roottable.deleted=0) as rowChecker";
            sql_string += "   where (rowChecker.parentkey is null OR rowChecker.ref_referral_plan$item is null)";
            DataTable Result = QueryRunSelect(sql_string, "Error-Query No Mammos or protocol", out errormsg);
            return Result;
        }

        private DataTable QueryCurrentButWithEndApptMammo(out string errormsg)
        {
            errormsg = "";
            string sql_string = "SELECT DISTINCT roottable.[demog$hospital_id],roottable.[demog$surname],roottable.[demog$forename],roottable.[demog$dob] FROM roottable";
            sql_string += " WHERE ( (roottable.[ref_status$item] = 0) AND (roottable.deleted = 0)  AND  ( roottable.[demog$hospital_id] in (";
            sql_string += "  SELECT  roottable.[demog$hospital_id] FROM mammo INNER JOIN roottable ON mammo.parentkey = roottable.primarykey";
            sql_string += "  WHERE ( (roottable.[ref_status$item] = 0)  AND (roottable.deleted = 0)  AND (mammo.deleted = 0) ";
            sql_string += " AND ((mammo.[status_mammo$item]=1) OR  (mammo.[status_mammo$item]=2)) ";
            sql_string += " AND ((mammo.[mammo_end_actioned$truefalse] is null) OR (mammo.[mammo_end_actioned$truefalse] = 0))  )  )  )  );";

            DataTable Result = QueryRunSelect(sql_string, "Error-Query Current But No 'WaitingRecall'", out errormsg);

            return Result;
        }


        private DataTable QueryNonCurrentButNoDischargeAndFutureApptsMammo(out string errormsg)
        {
            errormsg = "";
            string sql_string = "SELECT DISTINCT roottable.[demog$hospital_id],roottable.[demog$surname],roottable.[demog$forename],roottable.[demog$dob] FROM roottable";
            sql_string += " WHERE ( ((roottable.[ref_status$item] =1) OR (roottable.[ref_status$item] =2)) AND (roottable.deleted = 0) ";
           
            sql_string += " AND ( roottable.[demog$hospital_id] not in ( SELECT  roottable.[demog$hospital_id]";
            sql_string += " FROM mammo INNER JOIN roottable ON mammo.parentkey = roottable.primarykey";
            sql_string += " WHERE  (((roottable.[ref_status$item] =1) OR (roottable.[ref_status$item] =2)) AND (roottable.deleted = 0)  AND (mammo.deleted = 0) ";
            sql_string += " AND ( (mammo.[status_mammo$item]=2)OR(mammo.[status_mammo$item]=1))  )  )";
            
            sql_string += " AND ( roottable.[demog$hospital_id] in ( SELECT  roottable.[demog$hospital_id]";
            sql_string += " FROM mammo INNER JOIN roottable ON mammo.parentkey = roottable.primarykey";
            sql_string += " WHERE ( ((roottable.[ref_status$item] =1) OR (roottable.[ref_status$item] =2)) AND (roottable.deleted = 0)  AND (mammo.deleted = 0)";
            sql_string += "  AND( mammo.scan_date_mammo$date>'"+ DateTime.Now.ToString("yyyy-MM-dd") + "' ) AND ((mammo.status_mammo$item=6)or(mammo.status_mammo$item=7)) ) ) ) ) );";


            DataTable Result = QueryRunSelect(sql_string, "Error-Query Current But No 'WaitingRecall'", out errormsg);

            return Result;
        }

        private DataTable QueryRecentlyEnded(out string errormsg)
        {
            errormsg = "";
            DateTime startDate = DateTime.Now.AddMonths(-1);
            string sql_string = "SELECT [demog$hospital_id],[demog$surname],[demog$forename],[demog$dob],[ref_status$item],ref_discontinued$date,ref_discharged$date FROM roottable";
            sql_string += " WHERE ((([ref_status$item] = 1) AND (ref_discontinued$date > '"+startDate.ToString("yyyy-MM-dd")+"'))"; 
            sql_string += "     OR (([ref_status$item] = 2) AND (ref_discharged$date >   '"+startDate.ToString("yyyy-MM-dd")+"')))";
            sql_string += " AND (deleted = 0) AND (ref_autoendflag$truefalse = 1) ORDER BY [ref_status$item],ref_discontinued$date,ref_discharged$date";  
            DataTable Result = QueryRunSelect(sql_string, "Error-Query Current But No 'WaitingRecall'", out errormsg);

            return Result;
        }




        private DataTable QueryAllCurrentMammoWR(out string errormsg)
        {
            errormsg = "";

            string sql_string = "SELECT [roottable].[demog$hospital_id], [mammo].[status_mammo$item],[mammo].[scan_date_mammo$date],[roottable].[ref_next_mammo$date],[roottable].[primarykey]";
            sql_string += " FROM [breast_scanning].[dbo].[roottable] LEFT OUTER JOIN [breast_scanning].[dbo].[mammo] on [mammo].[parentkey]=[roottable].[primarykey]";
            sql_string += " WHERE [roottable].[ref_status$item]=0 ";
            sql_string += " AND [roottable].[deleted]=0 AND [mammo].[deleted] =0 ";
            sql_string += " AND (mammo.[status_mammo$item]=7 OR mammo.[status_mammo$item]=8)";
            //sql_string += " AND [mammo].[scan_date_mammo$date]!=[roottable].[ref_next_mammo$date]";
            sql_string += " ORDER BY [roottable].[demog$hospital_id] ASC, mammo.scan_date_mammo$date ASC";
            DataTable Result = QueryRunSelect(sql_string, "Error-Query All Current and next appts", out errormsg);

            return Result;
        }


        private DataTable QueryRunSelect(string selectQuery, string exceptionTag, out string msg)
        {

            msg = "";
            try
            {
                SqlConnection connection = new SqlConnection(ArQ_CFFH_intergration.Properties.Settings.Default.CFFH_connection_string);
                string sql_string = selectQuery;
                SqlDataAdapter adapter = new SqlDataAdapter(sql_string, connection);
                DataTable returnTable = new DataTable();
                adapter.Fill(returnTable);

                return returnTable;
            }
            catch (Exception ex1)
            {
                msg = exceptionTag + ": " + ex1.Message;
                return null;
            }
        }
        private DataTable setupRISTable(DataTable inTable,bool isCancelTab, bool addTypeCol)
        {

            inTable.Columns.Add("  Hospital ID  ", typeof(System.String));
            if (addTypeCol)
            {
                inTable.Columns.Add("  RIS Scan Date  ", typeof(System.DateTime));
            }
            else
            {
                inTable.Columns.Add("  ArQ Scan Date  ", typeof(System.DateTime));
            }
            if (addTypeCol)
            {
                inTable.Columns.Add("  Exam Type  ", typeof(System.String));
            }
            if (isCancelTab)
            {
                inTable.Columns.Add("  Scan Type  ", typeof(System.String));
            }
            else
            {
                inTable.Columns.Add("  RIS Reported Date  ", typeof(System.DateTime));
            }
            inTable.Columns.Add("  Location  ", typeof(System.String));
            if (isCancelTab)
            {
                inTable.Columns.Add("  Cancel Description  ", typeof(System.String));
            }
           
            return inTable;
        }
         private DataTable setupQueryTable(DataTable inTable)
         {
             inTable.Columns.Add("  Hospital ID  ", typeof(System.String));
             inTable.Columns.Add("  Forename  ", typeof(System.String));
             inTable.Columns.Add("  Surname  ", typeof(System.String));
             inTable.Columns.Add("  DOB  ", typeof(System.DateTime));
             return inTable;
         }
         private DataTable setupQueryTableEnd(DataTable inTable)
         {
             inTable.Columns.Add("  Hospital ID  ", typeof(System.String));
             inTable.Columns.Add("  Forename  ", typeof(System.String));
             inTable.Columns.Add("  Surname  ", typeof(System.String));
             inTable.Columns.Add("  DOB  ", typeof(System.DateTime));
             inTable.Columns.Add("  End Type  ", typeof(System.String));
             inTable.Columns.Add("  Date Ended  ", typeof(System.DateTime));
             return inTable;
         }
         private bool setFieldInTable(string tablename, int primarykey, string fieldname, DateTime fieldval, out string errorstring)
         {
             try
             {
                 SqlConnection connection = new SqlConnection(ArQ_CFFH_intergration.Properties.Settings.Default.CFFH_connection_string);
                 //StringBuilder sql = new StringBuilder("UPDATE [" + tablename + "] ([" + fieldname + "] = @fieldval ) where deleted = 0 AND primarykey=" + primarykey + ";");
                 string sqlString = "UPDATE [" + tablename + "] SET [" + fieldname + "] = @fieldval  WHERE deleted = 0 AND primarykey=" + primarykey + ";";

                 SqlCommand command = new SqlCommand(sqlString, connection);

                 // ArQ parent key.
                 command.Parameters.AddWithValue("@fieldval", fieldval.ToString("yyyy-MM-dd"));
                 connection.Open();
                 int count = command.ExecuteNonQuery();
                 connection.Close();
                 errorstring = "no errors";
                 return true;
             }
             catch (Exception ex)
             {
                 errorstring = ex.Message;
                 return false;
             }
         }
        //Overload for detection by unique hospitalID - needs to have Hosp ID as unique in ArQ 
         private bool setFieldInTable(string tablename, string hospIDFieldName,string hospIDValue, string fieldname, DateTime fieldval, out string errorstring)
         {
             try
             {
                 SqlConnection connection = new SqlConnection(ArQ_CFFH_intergration.Properties.Settings.Default.CFFH_connection_string);
                 //StringBuilder sql = new StringBuilder("UPDATE [" + tablename + "] ([" + fieldname + "] = @fieldval ) where deleted = 0 AND primarykey=" + primarykey + ";");
                 string sqlString = "UPDATE [" + tablename + "] SET [" + fieldname + "] = @fieldval  WHERE deleted = 0 AND "+ hospIDFieldName + "='" + hospIDValue + "';";

                 SqlCommand command = new SqlCommand(sqlString, connection);

                 // ArQ parent key.
                 command.Parameters.AddWithValue("@fieldval", fieldval.ToString("yyyy-MM-dd"));
                 connection.Open();
                 int count = command.ExecuteNonQuery();
                 connection.Close();
                 errorstring = "no errors";
                 return true;
             }
             catch (Exception ex)
             {
                 errorstring = ex.Message;
                 return false;
             }
         }
          

    }
}