﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CFFH_intergration
{
    class Program
    {
        static void Main(string[] args)
        {
            // ******* Process Command Line Parameters *********
            //         -cf##/##/#### = DateTime parsable string (improt clinic people and appointments from  - default=today


            int _maxdays = 40000;
            int _maxdaysBack = 50;
            bool _outputEmail = ArQ_CFFH_intergration.Properties.Settings.Default.useEmail;
            bool _outputHTML = ArQ_CFFH_intergration.Properties.Settings.Default.outHTMLSummaryPage;

            StringBuilder log = new StringBuilder();
            bool emailProblem = false;


            int nDays = 1;
            int backStartDays = 1;
            bool outWithResults = true;
            DateTime startDateImportWindow = DateTime.Now;
            DateTime endDateImportWindow = DateTime.Now;

            if (args != null)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    string comSubString1 = args[i].Substring(0, 3);
                    string comSubString2 = args[i].Substring(3);

                    if (string.Compare(comSubString1, "-cd", true) == 0) // -cd = temporal window size in days
                    {
                        if (!int.TryParse(comSubString2, out nDays))
                        {
                            log.AppendLine("Command Line parameter incorrect - either omit all params or use a valid date or the word 'Today'");
                            System.Environment.Exit(1);
                        }
                        else
                        {
                            if (nDays > _maxdays) { nDays = _maxdays; }
                        }
                    }
                    else if (string.Compare(comSubString1, "-cs", true) == 0) // -cs = num days back to start temporal window
                    {
                        if (!int.TryParse(comSubString2, out backStartDays))
                        {
                            log.AppendLine("Command Line parameter incorrect - either omit all params or use a valid date or the word 'Today'");
                            System.Environment.Exit(1);
                        }
                        else
                        {
                            if (backStartDays > _maxdaysBack) { backStartDays = _maxdaysBack; }
                        }
                    }
                    else if (string.Compare(comSubString1, "-oa", true) == 0) // -oa = output all - all appointments data mode
                    {
                        outWithResults = false;
                    }
                    else if (string.Compare(comSubString1, "-wp", true) == 0) // -wp = outputwebpage e.g. -wptrue
                    {
                        if (string.Compare(comSubString2, "true", true) == 0)
                        {
                            _outputHTML = true;
                        }
                        else if (string.Compare(comSubString2, "false", true) == 0)
                        {
                            _outputHTML = false;
                        }
                    }
                    else if (string.Compare(comSubString1, "-em", true) == 0) // -wp = outputwebpage e.g. -wptrue
                    {
                        if (string.Compare(comSubString2, "true", true) == 0)
                        {
                            _outputEmail = true;
                        }
                        else if (string.Compare(comSubString2, "false", true) == 0)
                        {
                            _outputEmail = false;
                        }
                    }


                    else if (string.Compare(comSubString1, "-h", true) == 0) // - = help
                    {
                        Console.WriteLine("Help: -cd## number of days to include in the window of time being used");
                        Console.WriteLine("      -cs## number of days back from now to end the window of time");
                        Console.WriteLine("      -oa switch to output all mode (used to get a ful list of all relevant booked)");
                        return;
                    }

                }
            }
            endDateImportWindow = endDateImportWindow.AddDays(-backStartDays);
            int datewinMinus = -(nDays);
            if (datewinMinus < 0)
            {
                startDateImportWindow = endDateImportWindow.AddDays(datewinMinus);
            }
            //
            if (outWithResults)
            {
                DoAudits auditer = new DoAudits();
                auditer.Go(nDays, backStartDays, true);

                // Email and log

                //string attendMsgBreath = attendStatusCount.Go(attendStatusCount.ArqConnectionString(), attendStatusCount.BreathTestEmptyAttendQuerySql(), "BREATH - ", "breathing test appointment(s) have no 'Attended Status' (excluding any for today).\n ");
                MPH.Utils.Email emailer = new MPH.Utils.Email();
                emailer.IsBodyHtml = true;

                //ClinicCodeSettings ccsBreath = ClinicCodeSettings.Default;

                string[] emailRecipients = ArQ_CFFH_intergration.Properties.Settings.Default.emailRecipients.Split(new char[] { ',' });

                if (emailRecipients.Length >= 1)
                {
                    for (int i = 0; i < emailRecipients.Length; i++)
                    {
                        emailer.AddToAddress(emailRecipients[i].Trim(), emailRecipients[i].Trim());
                    }
                }
                else
                {
                    log.AppendFormat("Problem with cffh email recipients: " + ArQ_CFFH_intergration.Properties.Settings.Default.emailRecipients);
                    emailProblem = true;
                }

                emailer.SetFromAddress("martin.bayley@sth.nhs.uk", "ArQ-Cancer Follow-up and Family History Screening");
                emailer.Subject = "ArQ-CFFH audits from date:" + startDateImportWindow.ToString("dd-MMM-yyyy") + " to present";
                emailer.Body += MPH.Utils.Email.HtmlEmailHeader();
                emailer.Body += "<hr/><h2>ArQ Patients With Results in RIS (scanned from " + startDateImportWindow.ToString("dd-MMM-yyyy") + " to " + endDateImportWindow.ToString("dd-MMM-yyyy") + ")</h2>";
                emailer.Body += "<pre>";
                log.AppendLine("ArQ Patients With Results Reported in RIS (scanned from " + startDateImportWindow.ToString("dd-MMM-yyyy") + " to " + endDateImportWindow.ToString("dd-MMM-yyyy") + ")");
                string storeThing = string.Empty;
                if (auditer.Get_CRIS_Results_Reminders.Length > 0)
                {
                    storeThing = auditer.Get_CRIS_Results_Reminders;
                    emailer.Body += "<p><span style='color: black'>" + storeThing + "</span></p>";
                    log.AppendLine(storeThing);
                }
                else
                {
                    emailer.Body += "None.";
                    log.AppendLine("None." + Environment.NewLine);
                }
                emailer.Body += "</pre><br/>";
                if (auditer.Get_Audit_Query_Warnings.Length > 0)
                {
                    emailer.Body += "<hr/><h2>ArQ Patients Flagged by Audit Queries (Warnings of detected inconsistencies)</h2>";
                    emailer.Body += "<pre><p>";
                    storeThing = auditer.Get_Audit_Query_Warnings;
                    emailer.Body += storeThing;
                    log.AppendLine("ArQ Patients Flagged by Audit Queries (Warnings of detected inconsistencies)");
                    log.AppendLine(storeThing);
                }
                else
                {
                    emailer.Body += "No Audit Issues Found.";
                    log.AppendLine("None." + Environment.NewLine);
                }
                emailer.Body += "</p></pre><br />";

                if (_outputHTML)
                {
                    string Message_of_day = ArQ_CFFH_intergration.Properties.Settings.Default.MessageOfTheDay;
                    string htlmlOut = emailer.Body;
                    string urgency = ArQ_CFFH_intergration.Properties.Settings.Default.HtmlUrgency;
                    htlmlOut = htlmlOut.Replace("<html><head>", "<html><head><link rel=\"stylesheet\" href=\"./breast_scanning.css\">");
                    htlmlOut = htlmlOut.Replace("body { font-family: Consolas, Calibri, Arial, Helvetica; font-size: 10pt;", "body { font-family: Consolas, Calibri, Arial, Helvetica; font-size: 13pt;");
                    htlmlOut = htlmlOut.Replace("h2 { color: #0066CC; font-weight: bold; font-size: 14pt;", "h2 { color: #0066CC; font-weight: bold; font-size: 15pt;");
                    htlmlOut = htlmlOut.Replace(" padding-bottom: 1px; font-size: 8pt;", " padding-bottom: 1px; font-size: 11pt;");
                    htlmlOut = htlmlOut.Replace("<body>", "<body><div class=\"motd urgency" + urgency + "\">" + Message_of_day + "</div><br /><p>(These lists are created daily - last created: "+DateTime.Now.ToString("dd/MMM/yyyy")+") </p><br />");
                    System.IO.StreamWriter file = new System.IO.StreamWriter(ArQ_CFFH_intergration.Properties.Settings.Default.HtmlPath);
                    //System.IO.StreamWriter file = new System.IO.StreamWriter("C:\\temp\\CFFH_audit\\index.html");
                    file.WriteLine(htlmlOut.ToString());
                    file.Flush();

                }
                emailer.Body += "<font size=\"small\" color=\"red\">This is an automated email - do not respond to it</font>";
                emailer.Body += MPH.Utils.Email.HtmlEmailFooter();
                if (_outputEmail)
                {
                    if (!emailProblem)
                    {
                        // strip out html hyperlinks for email...

                        string htmlText = emailer.Body;
                        bool contLoop=true; 
                        string chopString1,chopString2,IDString;
                        while(contLoop) 
                        {
                           
                            int pos1 = htmlText.IndexOf("<a href");
                          
                            if (pos1 == -1)
                            {
                                contLoop = false;
                            }
                            else
                            {
                                int pos2 = htmlText.IndexOf("</a>");
                                int lenChopOut = pos2 - pos1 - 6;
                                chopString1 = htmlText.Substring(0, pos1);
                                IDString = htmlText.Substring(pos1 + lenChopOut,6);
                                chopString2 = htmlText.Substring(pos2+4);
                                


                                htmlText = chopString1 +IDString+ chopString2;
                            }
                            
                        }
                        htmlText = htmlText.Replace("</a>", "");
                        emailer.Body = htmlText;
                        emailer.Send();
                    }
                    else
                    {
                        log.AppendLine("Problem with ArQ CFFH email.");
                    }
                }
                if (ArQ_CFFH_intergration.Properties.Settings.Default.OutLog)
                {
                    System.IO.StreamWriter file = new System.IO.StreamWriter(ArQ_CFFH_intergration.Properties.Settings.Default.LogfilePathAndName + DateTime.Now.ToString("yyyy_MM_dd") + ".log");
                    file.WriteLine(log.ToString());
                    file.Flush();
                }

            }
            else
            {
                DoAudits auditer = new DoAudits();
                auditer.Go(nDays, backStartDays, false);
                string storeThing = string.Empty;
                storeThing = auditer.Get_CRIS_Results_Reminders;
                System.IO.StreamWriter file = new System.IO.StreamWriter(ArQ_CFFH_intergration.Properties.Settings.Default.LogfilePathAndName + DateTime.Now.ToString("yyyy_MM_dd") + ".csv");
                file.WriteLine(storeThing.ToString());
                file.Flush();

            }
        }
    }
}